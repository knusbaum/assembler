OBJECTS=\
	labellist.o \
	lexer.o \
	parser.o \
	elf.o \
	map.o \
	buffwriter.o \
	log.o \
	main.o

EXECUTABLE=kas
KAS_OUT=kas_out
CFLAGS= -ggdb -Wall
LDFLAGS=

.phony: $(EXECUTABLE)

all : $(EXECUTABLE)
everything : $(EXECUTABLE) $(KAS_OUT) $(KAS_OUT)_static nasm_out
clean :
	-@rm *.o *.ko *~

nuke : clean
	-@rm nasm_out $(KAS_OUT) $(KAS_OUT)_static $(EXECUTABLE)

$(EXECUTABLE) : $(OBJECTS)
	cc $(LDFLAGS) -o $(EXECUTABLE) $(OBJECTS)

run : $(KAS_OUT)
	./$(KAS_OUT)

runstatic : $(KAS_OUT)_static
	./$(KAS_OUT)_static

$(KAS_OUT) : test.ko test2.ko
	ld -ggdb -melf_i386 -o $(KAS_OUT) test.ko test2.ko -lc --dynamic-linker=/lib/ld-linux.so.2

$(KAS_OUT)_static : test.ko test2.ko
	ld -ggdb -melf_i386 -L/usr/local/musl/lib/ -o $(KAS_OUT)_static test.ko test2.ko -static -lc

test.ko : test.ks $(EXECUTABLE)
	./kas -o test.ko test.ks

test2.ko : test2.ks $(EXECUTABLE)
	./kas -o test2.ko test2.ks

nasm_out : test.o
	ld -ggdb -melf_i386 -L /usr/lib32/ -lc --dynamic-linker=/lib/ld-linux.so.2 -o nasm_out test.o

test.o : test.s
	nasm -felf test.s

