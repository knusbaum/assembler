
#define SHF_WRITE              0x1
#define SHF_ALLOC              0x2
#define SHF_EXECINSTR          0x4
#define SHF_MASKPROC    0xf0000000

#define TYPE_NULL     0x00000000
#define TYPE_PROGBITS 0x00000001
#define TYPE_SYMTAB   0x00000002
#define TYPE_STRTAB   0x00000003
#define TYPE_RELA     0x00000004
#define TYPE_NOBITS   0x00000008
#define TYPE_REL      0x00000009


enum sym_binding {
    sym_local = 0,
    sym_global,
    sym_weak
};

enum sym_type {
    sym_notype = 0,
    sym_object,
    sym_func
};

typedef struct {
    char *name;
    uint32_t type;
    uint32_t flags;
    uint8_t *contents;
    uint32_t length;
    uint32_t entsize;
} section;

void add_symbol(char *symbol, uint32_t value, uint32_t size, enum sym_binding binding, enum sym_type type, uint16_t section_index);
bool add_reloc(char *symbol, uint32_t value, enum reloc_type type);
void write_elf_header(FILE *f, section *sections, uint32_t section_count, size_t local_syms);
