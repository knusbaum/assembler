#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "log.h"
#include "buffwriter.h"
#include "lexer.h"
#include "parser.h"
#include "elf.h"
#include "labellist.h"

static void write_little8(FILE *f, uint8_t val) {
    fwrite(&val, 1, 1, f);
}

static void write_little16(FILE *f, uint16_t val) {
    uint8_t values[2];
    values[0] = val & 0xFF;
    values[1] = (val >> 8) & 0xFF;
    fwrite(values, 1, 2, f);
}

static void write_little32(FILE *f, uint32_t val) {
    uint8_t values[4];
    values[0] = val & 0xFF;
    values[1] = (val >> 8) & 0xFF;
    values[2] = (val >> 16) & 0xFF;
    values[3] = (val >> 24) & 0xFF;
    fwrite(values, 1, 4, f);
}

void write_file_header(FILE *f, uint16_t section_count, uint16_t names_entry_idx) {
    char magic[] = { 0x7F, 'E', 'L', 'F' };
    char format32 = 1; // 2 for 64 bit
    char little_endian = 1; // 2 for big-endian
    char elf_vers = 1;
    char ABI = 0;
    char padding[8] = {0}; // 8 bytes padding
    /* 1 - relocatable
       2 - executable
       3 - shared
       4 - core */
    uint16_t relocatable = 1;
    uint16_t instr_set = 0x03; // x86
    uint32_t elf_vers2 = 1; // ? Same as above I guess.
    uint32_t entry_addr = 0; // No entry point. This is an object file.
    uint32_t program_header = 0; // no program header.
    uint32_t section_header = 52; // Start of section header is 52 bytes into the file.
    uint32_t flags = 0;
    uint16_t header_size = 52; // This header is 52-bytes long.
    uint16_t program_header_size = 0; // No program headers.
    uint16_t program_header_entry_count = 0; // "
    uint16_t section_header_size = 40; // Stole from readelf. Maybe need to change this.
    uint16_t section_header_entry_count = section_count; // Just .text and section names for now.
    uint16_t section_header_names_entry = names_entry_idx; // second (index 1) entry holds the names.

    fwrite(magic, 1, 4, f);                        // 0x00
    write_little8(f, format32);                    // 0x04
    write_little8(f, little_endian);               // 0x05
    write_little8(f, elf_vers);                    // 0x06
    write_little8(f, ABI);                         // 0x07
    fwrite(padding, 1, 8, f);                      // 0x08
    write_little16(f, relocatable);                // 0x10
    write_little16(f, instr_set);                  // 0x12
    write_little32(f, elf_vers2);                  // 0x14
    write_little32(f, entry_addr);                 // 0x18
    write_little32(f, program_header);             // 0x1C
    write_little32(f, section_header);             // 0x20
    write_little32(f, flags);                      // 0x24
    write_little16(f, header_size);                // 0x28
    write_little16(f, program_header_size);        // 0x2A
    write_little16(f, program_header_entry_count); // 0x2C
    write_little16(f, section_header_size);        // 0x2E
    write_little16(f, section_header_entry_count); // 0x30
    write_little16(f, section_header_names_entry); // 0x32
}

uint32_t total_section_len;
uint32_t name_off;

static void write_null_header(FILE *f) {
    int i;
    for(i = 0; i < 10; i++) {
        write_little32(f, 0);
    }
}

static void write_section_header(FILE *f, uint32_t total_sections, char *name, uint32_t type, uint32_t section_flags, uint32_t section_size, uint32_t link, uint32_t st_info, uint32_t align, uint32_t entsize) {
    // Write section headers
    uint32_t text_type = type;
    //uint32_t section_flags = section_flags; // Part of exec image, contains instructions.
    uint32_t address = 0;      // Relocatable. no address.
    uint32_t section_off = 52 + (total_sections * 40) + total_section_len; // File header plus two section headers.
    total_section_len += section_size;
    //uint8_t buffer_zone[16] = {0};

    if(name) {
        if(name_off == 0) name_off=1;
        write_little32(f, name_off);      // 0x00
        name_off += strlen(name) + 1;
    }
    else {
        write_little32(f, 0);
    }
    write_little32(f, text_type);     // 0x04
    write_little32(f, section_flags); // 0x08
    write_little32(f, address);       // 0x0C
    write_little32(f, section_off);   // 0x10
    write_little32(f, section_size);  // 0x14
    write_little32(f, link);          // 0x18
    write_little32(f, st_info);       // 0x1C
    write_little32(f, align);         // 0x20
    write_little32(f, entsize);       // 0x24
}

writable_buffer *sectionbuff;

static void add_section_name(char *section) {
    if(!sectionbuff) {
        sectionbuff = malloc(sizeof (writable_buffer));
        createbuffer(sectionbuff);
        buffer_write_byte(sectionbuff, 0);
    }

    int i;
    for(i = 0; i <= strlen(section); i++) {
        buffer_write_byte(sectionbuff, section[i]);
    }
}

writable_buffer *symbolnamebuff;

static void add_symbol_name(char *symbol) {
    if(!symbolnamebuff) {
        symbolnamebuff = malloc(sizeof (writable_buffer));
        createbuffer(symbolnamebuff);
        buffer_write_byte(symbolnamebuff, 0);
    }

    int i;
    for(i = 0; i <= strlen(symbol); i++) {
        buffer_write_byte(symbolnamebuff, symbol[i]);
    }
}

writable_buffer *symbolbuff;
struct lab_off *symbol_to_index;
size_t n_symbol;
void add_symbol(char *symbol, uint32_t value, uint32_t size, enum sym_binding binding, enum sym_type type, uint16_t section_index) {
    if(!symbol_to_index) {
        symbol_to_index = malloc(sizeof (struct lab_off));
        lablist_make(symbol_to_index);
        n_symbol = 1;
    }
    if(!symbolbuff) {
        symbolbuff = malloc(sizeof (writable_buffer));
        createbuffer(symbolbuff);
        int i;
        for(i = 0; i < 16; i++) {
            buffer_write_byte(symbolbuff, 0);
        }
    }

    // binding is irrelevant here.
    set_offset_for_label(symbol_to_index, symbol, n_symbol, 0);
    n_symbol++;

    size_t strindex = buffer_offset(symbolnamebuff);
    if(strindex == 0) strindex = 1;
    info("Writing symbol %s with offset %zu into .symtab\n", symbol, strindex);

    // st_name
    buffer_write_little32(symbolbuff, strindex);
    add_symbol_name(symbol);

    // st_value
    buffer_write_little32(symbolbuff, value);

    // st_size
    buffer_write_little32(symbolbuff, size);

    // st_info
    buffer_write_byte(symbolbuff, ((binding << 4) | type));

    // st_other
    buffer_write_byte(symbolbuff, 0);

    // st_shndx
    buffer_write_little16(symbolbuff, section_index);
}

writable_buffer *relocbuff;
bool add_reloc(char *symbol, uint32_t value, enum reloc_type type) {
    if(!relocbuff) {
        relocbuff = malloc(sizeof (writable_buffer));
        createbuffer(relocbuff);
    }
    buffer_write_little32(relocbuff, value);
    uint32_t st_info = 0;
    ssize_t idx = offset_for_label(symbol_to_index, symbol);

    if(idx == -1) {
        info("Failed to locate symbol %s. Adding to table.\n", symbol);
        add_symbol(symbol, 0, 0, sym_global, sym_notype, 0);
        idx = offset_for_label(symbol_to_index, symbol);
    }
    st_info = (idx << 8) | type;
    buffer_write_little32(relocbuff, st_info);
    return true;
}

// The number of non-user sections
#define N_AUTO_SECTIONS 5

void write_elf_header(FILE *f, section *sections, uint32_t section_count, size_t local_syms) {
    write_file_header(f, section_count+N_AUTO_SECTIONS, section_count + N_AUTO_SECTIONS-1);

    write_null_header(f);

    uint32_t text_section = 0;
    uint32_t i;
    for(i = 0; i < section_count; i++) {
        info("Writing header for section %s\n", sections[i].name);
        if(strcmp(sections[i].name, ".text") == 0) {
            text_section = i+1;
        }
        add_section_name(sections[i].name);
        write_section_header(f, section_count+N_AUTO_SECTIONS,
                             sections[i].name, sections[i].type,
                             sections[i].flags, sections[i].length,
                             0, 0, 16, sections[i].entsize);
    }

    info("FIRST NON-LOCAL SYM: %zu\n", local_syms);
    add_section_name(".symtab");
    write_section_header(f, section_count+N_AUTO_SECTIONS,
                         ".symtab", TYPE_SYMTAB, 0, buffer_offset(symbolbuff),
                         section_count+2, local_syms+1, 1, 16);

    add_section_name(".strtab");
    write_section_header(f, section_count+N_AUTO_SECTIONS,
                         ".strtab", TYPE_STRTAB, 0, buffer_offset(symbolnamebuff),
                         0, 0, 0, 0);

    add_section_name(".rel.text");
    write_section_header(f, section_count+N_AUTO_SECTIONS, ".rel.text",
                         TYPE_REL, 0, buffer_offset(relocbuff),
                         section_count+1, text_section, 4, 8);

    // Adding new auto sections here shouldn't break anything above if I adjust
    // the value of N_AUTO_SECTIONS.

    add_section_name(".shrstrtab");
    write_section_header(f, section_count+N_AUTO_SECTIONS, ".shrstrtab",
                         TYPE_STRTAB, 0, buffer_offset(sectionbuff),
                         0, 0, 0, 0);

    for(i = 0; i < section_count; i++) {
        info("Writing contents of section %s\n", sections[i].name);
        fwrite(sections[i].contents, 1, sections[i].length, f);
    }

    fwrite(symbolbuff->buff, 1, buffer_offset(symbolbuff), f);
    fwrite(symbolnamebuff->buff, 1, buffer_offset(symbolnamebuff), f);
    if(relocbuff) {
        fwrite(relocbuff->buff, 1, buffer_offset(relocbuff), f);
    }
    fwrite(sectionbuff->buff, 1, buffer_offset(sectionbuff), f);
}
