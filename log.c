#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include "log.h"

bool logp;

void set_logging(bool should_log) {
    logp = should_log;
}

void info(char *fmt, ...) {
    if(logp) {
        va_list argp;
        va_start(argp, fmt);
        vfprintf(stderr, fmt, argp);
        va_end(argp);
    }
}
