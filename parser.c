#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "log.h"
#include "buffwriter.h"
#include "lexer.h"
#include "parser.h"
#include "elf.h"
#include "labellist.h"
#include "map.h"

#define r_EAX 0b000
#define r_ECX 0b001
#define r_EDX 0b010
#define r_EBX 0b011
#define r_ESP 0b100
#define r_EBP 0b101
#define r_ESI 0b110
#define r_EDI 0b111

#define MOD_INDIRECT        0b00
#define MOD_INDIRECT_DISP8  0b01
#define MOD_INDIRECT_DISP32 0b10
#define MOD_DIRECT          0b11

#define RM_SIB    0b100
#define RM_DISP32 0b101

// Can be indexed with registers enum members.
uint8_t regnums[] = {r_EAX, r_EBX, r_ECX, r_EDX, r_ESI, r_EDI, r_ESP, r_EBP};

struct parse_section *parse_section_make(char *name) {
    struct parse_section *s = malloc(sizeof (struct parse_section));
    s->name = strdup(name);
    s->lablist = lablist_make();
    s->relocs = map_make();
    s->curroffset = 0;
    createbuffer(&s->wb);
    return s;
}

void parse_section_destroy(struct parse_section *s) {
    lablist_destroy(s->lablist);
    map_destroy(s->relocs);
    free(s->name);
    destroybuffer(&s->wb);
    free(s);
}

struct parse_section *curr_p_section;
struct map *parse_sections;

static bool parse_labeldef(nexttok_t nexttok, token *label) {
    set_offset_for_label(curr_p_section->lablist, label->label, curr_p_section->curroffset, sym_local);
    info("(%s) Label %s @ offset 0x%lX\n",
            curr_p_section->name, label->label, curr_p_section->curroffset);
    return true;
}

static uint8_t mk_mod_reg_r_m(uint8_t mod, uint8_t reg, uint8_t r_m) {
    return (mod << 6 |
            reg << 3 |
            r_m);
}

// This is the same as write_opcode. It just reads clearer in code.
static void write_data_byte(uint8_t byte) {
    buffer_write_byte(&curr_p_section->wb, byte);
    curr_p_section->curroffset++;
}

static void write_opcode(uint8_t opcode) {
    buffer_write_byte(&curr_p_section->wb, opcode);
    curr_p_section->curroffset++;
}

static void write_opcode_imm32(uint8_t opcode, uint32_t imm) {
    write_opcode(opcode);
    uint32_t i;
    for(i = 0; i < 32; i+= 8) {
        buffer_write_byte(&curr_p_section->wb, imm >> i);
    }
    curr_p_section->curroffset += sizeof (uint32_t);
}

static void write_opcode_imm8(uint8_t opcode, uint8_t imm) {
    write_opcode(opcode);
    buffer_write_byte(&curr_p_section->wb, imm);
    curr_p_section->curroffset++;
}

static void write_instr(uint8_t opcode, uint8_t mod, uint8_t reg, uint8_t r_m) {
    uint8_t mod_reg_r_m = mk_mod_reg_r_m(mod, reg, r_m);
    write_opcode(opcode);
    buffer_write_byte(&curr_p_section->wb, mod_reg_r_m);
    curr_p_section->curroffset++;
}

static void write_instr_imm8(uint8_t opcode, uint8_t mod, uint8_t reg, uint8_t r_m, uint8_t imm) {
    write_instr(opcode, mod, reg, r_m);
    buffer_write_byte(&curr_p_section->wb, imm);
    curr_p_section->curroffset++;
}

static void write_instr_imm32(uint8_t opcode, uint8_t mod, uint8_t reg, uint8_t r_m, uint32_t imm) {
    write_instr(opcode, mod, reg, r_m);
    uint32_t i;
    for(i = 0; i < 32; i+= 8) {
        buffer_write_byte(&curr_p_section->wb, imm >> i);
    }
    curr_p_section->curroffset += sizeof (uint32_t);
}

static void write_instr_sib(uint8_t opcode, uint8_t mod, uint8_t reg, uint8_t r_m, uint8_t scale, uint8_t index, uint8_t base) {
    uint8_t sib = mk_mod_reg_r_m(scale, index, base);
    write_instr(opcode, mod, reg, r_m);
    write_data_byte(sib);
}

static bool parse_add(nexttok_t nexttok) {
    token arg1;
    token arg2;
    if(! (nexttok(&arg1) && nexttok(&arg2))) {
        return false;
    }

    if(arg2.type != REGISTER) {
        fprintf(stderr, "(Line %d) Expecting register as second arg of add instruction.\n", currline());
        fprintf(stderr, "Got arg1: ");
        print_token(&arg1);
        fprintf(stderr, "arg2: ");
        print_token(&arg2);
        fprintf(stderr, "\n");
        return false;
    }

    info("Emitting code for: [ADD ");
    info_token(&arg1);
    info_token(&arg2);
    info("]\n");

    switch(arg1.type) {
    case REGISTER:
        write_instr(0x01, MOD_DIRECT, regnums[arg1.reg], regnums[arg2.reg]);
        return true;
        break;
    case IMMEDIATE:
        write_instr_imm32(0x81, MOD_DIRECT, 0, regnums[arg2.reg], arg1.data);
        return true;
        break;
    default:
        fprintf(stderr, "(Line %d) Cannot encode instruction: [ADD ", currline());
        print_token(&arg1);
        print_token(&arg2);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    fprintf(stderr, "Done.\n");
    return false;
}

static bool parse_sub(nexttok_t nexttok) {
    token arg1;
    token arg2;
    if(! (nexttok(&arg1) && nexttok(&arg2))) {
        return false;
    }

    if(arg2.type != REGISTER) {
        fprintf(stderr, "(Line %d) Expecting register as second arg of sub instruction.\n", currline());
        fprintf(stderr, "Got arg1: ");
        print_token(&arg1);
        fprintf(stderr, "arg2: ");
        print_token(&arg2);
        fprintf(stderr, "\n");
        return false;
    }

    info("Emitting code for: [SUB ");
    info_token(&arg1);
    info_token(&arg2);
    info("]\n");

    switch(arg1.type) {
    case REGISTER:
        write_instr(0x2B, MOD_DIRECT, regnums[arg1.reg], regnums[arg2.reg]);
        return true;
        break;
    case IMMEDIATE:
        write_instr_imm32(0x81, MOD_DIRECT, 5, regnums[arg2.reg], arg1.data);
        return true;
        break;
    default:
        fprintf(stderr, "(Line %d) Cannot encode instruction: [SUB ", currline());
        print_token(&arg1);
        print_token(&arg2);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    return false;
}

static bool parse_mov(nexttok_t nexttok) {
    token arg1;
    token arg2;
    if(! (nexttok(&arg1) && nexttok(&arg2))) {
        return false;
    }

    if(arg2.type != REGISTER) {
        fprintf(stderr, "(Line %d) Expecting register as second arg of mov instruction.\n", currline());
        fprintf(stderr, "Got arg1: ");
        print_token(&arg1);
        fprintf(stderr, "arg2: ");
        print_token(&arg2);
        fprintf(stderr, "\n");
        return false;
    }

    info("Emitting code for: [MOV ");
    info_token(&arg1);
    info_token(&arg2);
    info("]\n");
    struct reloc *rel;
    switch(arg1.type) {
    case REGISTER_D:
        // Check if the RM bits would conflict with special modes and
        // emit different codes.
        if(regnums[arg1.reg] == RM_SIB) {
            // ESP
            write_instr_sib(0x8b, MOD_INDIRECT, regnums[arg2.reg], RM_SIB, 0x0, regnums[arg1.reg], regnums[arg1.reg]);
        }
        else if(regnums[arg1.reg] == RM_DISP32) {
            // EBP
            write_instr_imm8(0x8b, MOD_INDIRECT_DISP8, regnums[arg2.reg], RM_DISP32, 0);
        }
        else {
            write_instr(0x8b, MOD_INDIRECT, regnums[arg2.reg], regnums[arg1.reg]);
        }
        return true;
        break;
    case REGISTER:
        write_instr(0x89, MOD_DIRECT, regnums[arg1.reg], regnums[arg2.reg]);
        return true;
        break;
    case IMMEDIATE:
        write_opcode_imm32(0xB8 + regnums[arg2.reg], arg1.data);
        return true;
        break;
    case LABEL_D:
        write_instr_imm32(0x8b, MOD_INDIRECT, regnums[arg2.reg], RM_DISP32, 0);
        rel = malloc(sizeof (struct reloc));
        rel->offset = curr_p_section->curroffset-4;
        rel->type = R_386_32;
        map_put(curr_p_section->relocs, arg1.label, rel);
        return true;
        break;
    case LABEL:
        write_opcode_imm32(0xB8 + regnums[arg2.reg], 0);
        rel = malloc(sizeof (struct reloc));
        rel->offset = curr_p_section->curroffset-4;
        rel->type = R_386_32;
        map_put(curr_p_section->relocs, arg1.label, rel);
        return true;
        break;
    default:
        fprintf(stderr, "(Line %d) Cannot encode instruction: [MOV ", currline());
        print_token(&arg1);
        print_token(&arg2);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    return false;
}

static bool parse_jmp(nexttok_t nexttok) {
    token arg1;
    if(!nexttok(&arg1)) {
        return false;
    }

    info("Emitting code for: [JMP ");
    info_token(&arg1);
    info("]\n");

    switch(arg1.type) {
    case LABEL:
        write_opcode_imm32(0xE9, -4);
        struct reloc *rel = malloc(sizeof (struct reloc));
        rel->offset = curr_p_section->curroffset-4;
        rel->type = R_386_PC32;
        map_put(curr_p_section->relocs, arg1.label, rel);
        return true;
        break;
    default:
        fprintf(stderr, "(Line %d) Cannot encode instruction: [JMP ", currline());
        print_token(&arg1);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    return false;
}

static bool parse_cmp(nexttok_t nexttok) {
    token arg1;
    token arg2;
    if(! (nexttok(&arg1) && nexttok(&arg2))) {
        return false;
    }

    if(arg1.type != REGISTER) {
        fprintf(stderr, "(Line %d) Expecting register as first arg of cmp instruction.\n", currline());
        fprintf(stderr, "Got arg1: ");
        print_token(&arg1);
        fprintf(stderr, "arg2: ");
        print_token(&arg2);
        fprintf(stderr, "\n");
        return false;
    }

    info("Emitting code for: [CMP ");
    info_token(&arg1);
    info_token(&arg2);
    info("]\n");

    switch(arg2.type) {
    case REGISTER:
        write_instr(0x39, MOD_DIRECT, regnums[arg1.reg], regnums[arg2.reg]);
        return true;
        break;
    case IMMEDIATE:
        write_instr_imm32(0x81, MOD_DIRECT, 0x7, regnums[arg1.reg], arg2.data);
        return true;
        break;
    default:
        fprintf(stderr, "(Line %d) Cannot encode instruction: [CMP ", currline());
        print_token(&arg1);
        print_token(&arg2);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    return false;
}

static bool parse_jne(nexttok_t nexttok) {
    token arg1;
    if(!nexttok(&arg1)) {
        return false;
    }

    info("Emitting code for: [JNE ");
    info_token(&arg1);
    info("]\n");

    switch(arg1.type) {
    case LABEL:
        write_opcode(0x0F);
        write_opcode_imm32(0x85, -4);
        struct reloc *rel = malloc(sizeof (struct reloc));
        rel->offset = curr_p_section->curroffset-4;
        rel->type = R_386_PC32;
        map_put(curr_p_section->relocs, arg1.label, rel);
        return true;
        break;
    default:
        fprintf(stderr, "(Line %d) Cannot encode instruction: [JNE ", currline());
        fprintf(stderr, "type: %d\n", arg1.type);
        print_token(&arg1);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    return false;
}

static bool parse_je(nexttok_t nexttok) {
    token arg1;
    if(!nexttok(&arg1)) {
        return false;
    }

    info("Emitting code for: [JE ");
    info_token(&arg1);
    info("]\n");

    switch(arg1.type) {
    case LABEL:
        write_opcode(0x0F);
        write_opcode_imm32(0x84, -4);
        struct reloc *rel = malloc(sizeof (struct reloc));
        rel->offset = curr_p_section->curroffset-4;
        rel->type = R_386_PC32;
        map_put(curr_p_section->relocs, arg1.label, rel);
        return true;
        break;
    default:
        fprintf(stderr, "(Line %d) Cannot encode instruction: [JE ", currline());
        print_token(&arg1);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    return false;
}

static bool parse_push(nexttok_t nexttok) {
    token arg1;
    if(!nexttok(&arg1)) {
        return false;
    }

    info("Emitting code for: [PUSH ");
    info_token(&arg1);
    info("]\n");

    switch(arg1.type) {
    case REGISTER:
        write_opcode(0x50 + regnums[arg1.reg]);
        return true;
        break;
    case IMMEDIATE:
        write_opcode_imm32(0x68, arg1.data);
        return true;
        break;
    case LABEL:
        write_opcode_imm32(0x68, 0);
        struct reloc *rel = malloc(sizeof (struct reloc));
        rel->offset = curr_p_section->curroffset-4;
        rel->type = R_386_32;
        map_put(curr_p_section->relocs, arg1.label, rel);
        return true;
        break;
    default:
        fprintf(stderr, "Cannot encode instruction: [PUSH ");
        print_token(&arg1);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    return false;
}

static bool parse_pop(nexttok_t nexttok) {
    token arg1;
    if(!nexttok(&arg1)) {
        return false;
    }

    info("Emitting code for: [POP ");
    info_token(&arg1);
    info("]\n");

    switch(arg1.type) {
    case REGISTER:
        write_opcode(0x58 + regnums[arg1.reg]);
        return true;
        break;
    default:
        fprintf(stderr, "(Line %d) Cannot encode instruction: [POP ", currline());
        print_token(&arg1);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    return false;
}

static bool parse_call(nexttok_t nexttok) {
    token arg1;
    if(!nexttok(&arg1)) {
        return false;
    }

    info("Emitting code for: [CALL ");
    info_token(&arg1);
    info("]\n");

    switch(arg1.type) {
    case LABEL:
        write_opcode_imm32(0xE8, -4);
        struct reloc *rel = malloc(sizeof (struct reloc));
        rel->offset = curr_p_section->curroffset-4;
        rel->type = R_386_PC32;
        map_put(curr_p_section->relocs, arg1.label, rel);
        return true;
        break;
    default:
        fprintf(stderr, "(Line %d) Cannot encode instruction: [CALL ", currline());
        print_token(&arg1);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    return false;
}

static bool parse_ret(nexttok_t nexttok) {
    write_opcode(0xC3);
    return true;
}

static bool parse_int(nexttok_t nexttok) {
    token arg1;
    if(!nexttok(&arg1)) {
        return false;
    }

    info("Emitting code for: [INT ");
    info_token(&arg1);
    info("]\n");

    switch(arg1.type) {
    case REGISTER:
        fprintf(stderr, "Register INT not implemented.");
        return false;
        break;
    case IMMEDIATE:
        write_opcode_imm8(0xCD, arg1.data);
        return true;
        break;
    default:
        fprintf(stderr, "(Line %d) Cannot encode instruction: [INT ", currline());
        print_token(&arg1);
        fprintf(stderr, "]\n");
        return false;
        break;
    }
    return false;
}

static bool parse_shl(nexttok_t nexttok) {
    token arg1;
    token arg2;
    if(! (nexttok(&arg1) && nexttok(&arg2))) {
        return false;
    }

    if(arg1.type != REGISTER) {
        fprintf(stderr, "(Line %d) Expecting register as first arg of shl instruction.\n", currline());
        fprintf(stderr, "Got arg1: ");
        print_token(&arg1);
        fprintf(stderr, "arg2: ");
        print_token(&arg2);
        fprintf(stderr, "\n");
        return false;
    }
    else if(arg2.type != IMMEDIATE) {
        fprintf(stderr, "(Line %d) Expecting immediate value as second arg of shl instruction.\n", currline());
        fprintf(stderr, "Got: ");
        print_token(&arg2);
        fprintf(stderr, "\n");
        return false;
    }

    info("Emitting code for: [SHL ");
    info_token(&arg1);
    info_token(&arg2);
    info("]\n");

    write_instr_imm8(0xC1, MOD_DIRECT, 4, regnums[arg1.reg], arg2.data);
    return true;
}

static bool parse_section(nexttok_t nexttok) {
    token arg1;
    if(!nexttok(&arg1)) {
        fprintf(stderr, "(Line %d) Failed to get section arg.\n", currline());
        return false;
    }
    if(arg1.type != LABEL) {
        fprintf(stderr, "(Line %d) Invalid section.\n", currline());
    }
    struct parse_section *section = map_get(parse_sections, arg1.label);
    if(section == NULL) {
        section = parse_section_make(arg1.label);
        map_put(parse_sections, arg1.label, section);
    }
    info("Switching to section (%s)\n", arg1.label);
    curr_p_section = section;
    return true;
}

static bool parse_global(nexttok_t nexttok) {
    token arg1;
    if(!nexttok(&arg1)) {
        fprintf(stderr, "(Line %d) Failed to get global arg.\n", currline());
        return false;
    }
    if(arg1.type != LABEL) {
        fprintf(stderr, "(Line %d) Invalid section.\n", currline());
    }

    set_offset_for_label(curr_p_section->lablist, arg1.label, 0, sym_global);
    return true;
}

static bool parse_command(nexttok_t nexttok, token *tok) {
    bool ret = false;
    switch(tok->command) {
    case ADD:
        ret = parse_add(nexttok);
        break;
    case SUB:
        ret = parse_sub(nexttok);
        break;
    case MOV:
        ret = parse_mov(nexttok);
        break;
    case JMP:
        ret = parse_jmp(nexttok);
        break;
    case CMP:
        ret = parse_cmp(nexttok);
        break;
    case JNE:
        ret = parse_jne(nexttok);
        break;
    case JE:
        ret = parse_je(nexttok);
        break;
    case PUSH:
        ret = parse_push(nexttok);
        break;
    case POP:
        ret = parse_pop(nexttok);
        break;
    case CALL:
        ret = parse_call(nexttok);
        break;
    case RET:
        ret = parse_ret(nexttok);
        break;
    case INT:
        ret = parse_int(nexttok);
        break;
    case SHL:
        ret = parse_shl(nexttok);
        break;
    case SECTION:
        ret = parse_section(nexttok);
        break;
    case GLOBAL:
        ret = parse_global(nexttok);
        break;
    default:
        fprintf(stderr, "Invalid token command id: [%u]. Something is funny. Line %d\n", tok->command, currline());
        ret = false;
        break;
    }
    if(!ret) {
        fprintf(stderr, "Failed to parse command ");
        print_token(tok);
        fprintf(stderr, "\n");
    }
    return ret;
}

static bool parse_db(token *tok) {
    unsigned char *curr = tok->buffer->buff;
    while(curr < tok->buffer->buff_ptr) {
        write_data_byte(*curr);
        curr++;
    }
    return true;
}

void init_parse() {
    parse_sections = map_make();
    curr_p_section = parse_section_make(".text");
    map_put(parse_sections, ".text", curr_p_section);
}

bool parse(nexttok_t nexttok) {
    //fprintf(stderr, "Doing another parse.\n");
    token t;
    if(!nexttok(&t)) {
        //fprintf(stderr, "Failed to get another token.\n");
        return false;
    }

    while(t.type == NEWLINE) {
        if(!nexttok(&t)) {
            return false;
        }
    }

    bool ret = false;
    //fprintf(stderr, "Switching on token type.\n");
    switch(t.type) {
    case LABELDEF:
        ret = parse_labeldef(nexttok, &t);
        break;
    case COMMAND:
        ret = parse_command(nexttok, &t);
        break;
    case DATA:
        ret = parse_db(&t);
        break;
    default:
        fprintf(stderr, "(Line %d) Expected label or instruction, but got: ", currline());
        print_token(&t);
        fprintf(stderr, "t.type == %d\n", t.type);
        break;
    }
    //fprintf(stderr, "Flushing.\n");

    //fprintf(stderr, "Get the newline token.\n");
    if(!nexttok(&t)) {
        return ret;
    }

    if(t.type != NEWLINE) {
        fprintf(stderr, "Expected newline, but got: ");
        print_token(&t);
        fprintf(stderr, "\n");
        return false;
    }
    //fprintf(stderr, "Returning %d.\n", ret);
    return ret;
}

struct map *get_parse_section_map() {
    return parse_sections;
}
