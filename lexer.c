#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include "log.h"
#include "buffwriter.h"
#include "lexer.h"

// Lines (and certainly tokens) shouldn't be more than 128 chars long.
#define MAX_LINE 128

bool eof;
unsigned char Look;
unsigned int line = 0, col = 0;

unsigned char linebuff[MAX_LINE];

static void print_line(FILE *f) {
    fprintf(f, "%s\n", linebuff);
    int i;
    for(i = 0; i < col; i++) {
        fprintf(f, " ");
    }
    fprintf(f, "^\n");
}

bool is_eof() {
    return eof;
}

unsigned int currline() {
    return line;
}

bool get_char(int fd) {
    if(Look == '\n') {
        line++;
        col = 0;
    }
    else {
        col++;
    }
    bool ret;
    ret = read(fd, &Look, 1);
    if(Look == '#') {
        while(Look != '\n') {
            ret = read(fd, &Look, 1);
            if(ret != 1)
                return 0;
        }
    }
    if(Look != '\n')
    {
        linebuff[col] = Look;
        linebuff[col+1] = 0;
    }
    eof = !ret;
    return ret;
}

bool is_divider(char c) {
    return (c == '\n' ||
            c == '\t' ||
            c == ' ' ||
            c == ',');
}

bool init_lex(int fd) {
    line = 1;
    col = 0;
    return get_char(fd);
}

void consume_whitespace(int fd) {
    while(Look == ' ' ||
          Look == '\t') {
        get_char(fd);
    }
}

void consume_whitespace_or_comma(int fd) {
    while(Look == ',' ||
          Look == ' ' ||
          Look == '\t') {
        get_char(fd);
    }
}

int parse_hex_char(char c) {
    if(c >= '0' && c <= '9') {
        return c - '0';
    }
    switch(c) {
    case 'A':
    case 'a':
        return 10;
        break;
    case 'B':
    case 'b':
        return 11;
        break;
    case 'C':
    case 'c':
        return 12;
        break;
    case 'D':
    case 'd':
        return 13;
        break;
    case 'E':
    case 'e':
        return 14;
        break;
    case 'F':
    case 'f':
        return 15;
        break;
    default:
        // Not a hex char.
        return -1;
        break;
    }
}

static bool read_buffer(int fd, writable_buffer *wb) {
    consume_whitespace(fd);
    if(Look == ',') {
        get_char(fd);
    }
    if(Look == '\n') {
        fprintf(stderr, "(Line %d) Expecting data in declaration.\n", line);
        print_line(stderr);
        return false;
    }

    if(Look == '"') {
        // DB strings can span multiple lines, but newlines are interpreted
        // literally (they end up in the data)
        get_char(fd);
        while(Look != '"') {
            buffer_write_byte(wb, Look);
            get_char(fd);
        }
        get_char(fd);
        return true;
    }
    else {
        // DB hex literals must be in two digit pairs optionally separated by
        // whitespace (no newline)
        // Try to parse a hex number
        while(Look != '\n' && Look != ',') {
            consume_whitespace(fd);
            int hi = parse_hex_char(Look);
            if(hi == -1) {
                fprintf(stderr, "(Line %d) Expecting hex literal data. Found '%c'\n", line, Look);
                print_line(stderr);
                return false;
            }
            get_char(fd);
            int lo = parse_hex_char(Look);
            if(lo == -1) {
                if(Look == ' ') {
                    fprintf(stderr, "(Line %d) Hex literals must come in two-digit pairs optionally separated by whitespace.\n", line);
                    print_line(stderr);
                }
                else {
                    fprintf(stderr, "(Line %d) Expecting hex literal data. Found '%c'\n", line, Look);
                    print_line(stderr);
                }
                return false;
            }
            get_char(fd);
            unsigned char num = (hi << 4) | lo;
            buffer_write_byte(wb, num);
            consume_whitespace(fd);
        }
        return true;
    }
}

char *str_commands[] = { "add", "sub", "mov", "jmp", "cmp", "jne", "je", "push", "pop", "call", "ret", "int", "shl", "section", "db", "global", NULL };
char *str_registers[] = { "EAX", "EBX", "ECX", "EDX", "ESI", "EDI", "ESP", "EBP", NULL };

bool read_token(int fd, token *tok) {
    if(Look == 0) {
        init_lex(fd);
    }

    consume_whitespace_or_comma(fd);

    if(Look == '\n') {
        if(!get_char(fd)) {
            return false;
        }
        tok->type = NEWLINE;
        return true;
    }

    char buffer[MAX_LINE];
    bool found_divider = false;
    int i;
    for(i = 0; i < MAX_LINE; i++) {
        if(is_divider(Look)) {
            found_divider = true;
            break;
        }
        else {
            buffer[i] = Look;
        }

        if(!get_char(fd)) {
            Look = 0;
            break;
        }
    }
    //printf("\n");
    buffer[i++] = 0;
    //printf("Scanned [%s]\n", buffer);

    if(!found_divider) {
        fprintf(stderr, "Line too long: %d\n", line);
        print_line(stderr);
        return false;
    }

    bool deref = false;
    //printf("Checking if it's a deref.\n");
    if(buffer[0] == '[') {
        deref = true;
        memmove(buffer, buffer+1, MAX_LINE-1);
        int i;
        bool matched_bracket = false;
        for(i = 0; i < MAX_LINE; i++) {
            if(buffer[i] == ']'){
                buffer[i] = 0;
                matched_bracket = true;
            }
        }
        if(!matched_bracket) {
            fprintf(stderr, "(Line %d) Brackets not closed. Expecting ']'.\n", line);
            return false;
        }
    }
    //printf("un-derefed: [%s]\n", buffer);

    //printf("Checking if it's a command.\n");
    // See if it's a command.
    for(i = 0; str_commands[i] != NULL; i++) {
        if(strcasecmp(buffer, str_commands[i]) == 0) {
            if(deref) {
                fprintf(stderr, "(Line %d) invalid command!\n", line);
                print_line(stderr);
                return false;
            }
            tok->type = COMMAND;
            tok->command = (enum commands)i;
            if(tok->command == DB) {
                tok->buffer = malloc(sizeof (writable_buffer));
                createbuffer(tok->buffer);
                tok->type = DATA;
                do {
                    if(!read_buffer(fd, tok->buffer)) {
                        destroybuffer(tok->buffer);
                        free(tok->buffer);
                        return false;
                    }
                } while(Look == ',');
            }
            //printf("It is!\n");
            return true;
        }
    }
    //printf("Done.\n");

    //printf("Checking if it's a register.\n");
    // See if it's a register.
    for(i = 0; str_registers[i] != NULL; i++) {
        if(strcasecmp(buffer, str_registers[i]) == 0) {
            if(deref) {
                tok->type = REGISTER_D;
            }
            else {  
                tok->type = REGISTER;
            }
            tok->reg = (enum registers)i;
            //printf("It is!\n");
            return true;
        }
    }
    //printf("Done.\n");

    //printf("Checking if it's an immediate.\n");
    unsigned int data;
    //Check if it's an immediate.
    //Check for hex
    if((strlen(buffer) >=3 // At least big enough for 0x0
        && buffer[0] == '0' // Begins with 0x
        && buffer[1] == 'x'
        && sscanf(buffer+2, "%x", &data)) // Can scan the rest as hex
       // Check for dec
       || sscanf(buffer, "%d", &data)){
        if(deref) {
            fprintf(stderr, "(Line %d) invalid literal!\n", line);
            print_line(stderr);
            return false;
        }
        tok->type = IMMEDIATE;
        tok->data = data;
        //printf("It is!\n");
        return true;
    }
    //printf("Done.\n");

    //printf("Checking if it's a label def.\n");
    // Check label
    size_t len = strlen(buffer);
    if(buffer[len-1] == ':') {
        buffer[len-1] = 0;
        if(deref) {
            fprintf(stderr, "(Line %d) invalid label!\n", line);
            print_line(stderr);
            return false;
        }
        tok->type = LABELDEF;
        tok->label = strdup(buffer);
        //printf("It is!\n");
        return true;
    }
    //printf("Done.\n");

    //printf("Checking if it's a label.\n");
    if(isalpha(buffer[0]) || buffer[0] == '.' || buffer[0] == '_') {
        if(deref) {
            tok->type = LABEL_D;
        }
        else {
            tok->type = LABEL;
        }
        tok->label = strdup(buffer);
        //printf("It is!\n");
        return true;
    }
    //printf("Done.\n");

    fprintf(stderr, "I don't know this token is! [%s]\n", buffer);
    return false;
}

void info_token(token *tok) {
    switch(tok->type) {
    case LABEL:
    case LABELDEF:
        info("[LABEL: %s] ", tok->label);
        break;
    case LABEL_D:
        info("[LABEL (indirect): [%s]] ", tok->label);
        break;
    case COMMAND:
        info("[COMMAND: %s] ", str_commands[tok->command]);
        break;
    case REGISTER:
        info("[REGISTER: %s] ", str_registers[tok->reg]);
        break;
    case REGISTER_D:
        info("[REGISTER (indirect): [%s]] ", str_registers[tok->reg]);
        break;
    case IMMEDIATE:
        info("[IMMEDIATE: %d] ", tok->data);
        break;
    case NEWLINE:
        info("[NEWLINE] ");
        break;
    default:
        info("[?????] ");
        break;
    }
}

void print_token(token *tok) {

    switch(tok->type) {
    case LABEL:
    case LABELDEF:
        fprintf(stderr, "[LABEL: %s] ", tok->label);
        break;
    case LABEL_D:
        fprintf(stderr, "[LABEL (indirect): [%s]] ", tok->label);
        break;
    case COMMAND:
        fprintf(stderr, "[COMMAND: %s] ", str_commands[tok->command]);
        break;
    case REGISTER:
        fprintf(stderr, "[REGISTER: %s] ", str_registers[tok->reg]);
        break;
    case REGISTER_D:
        fprintf(stderr, "[REGISTER (indirect): [%s]] ", str_registers[tok->reg]);
        break;
    case IMMEDIATE:
        fprintf(stderr, "[IMMEDIATE: %d] ", tok->data);
        break;
    case NEWLINE:
        fprintf(stderr, "[NEWLINE] ");
        break;
    default:
        fprintf(stderr, "[?????] ");
        break;
    }
}
