
struct parse_section {
    char *name;
    struct lab_off *lablist;
    struct map *relocs; // sym -> reloc
    size_t curroffset;
    writable_buffer wb;
//    uint8_t *buff;
//    uint8_t *buff_ptr;
//    size_t buff_len;
};

enum reloc_type {
	R_386_NONE		= 0, // No relocation
	R_386_32		= 1, // Symbol + Offset
	R_386_PC32		= 2  // Symbol + Offset - Section Offset
};

struct reloc {
    uint32_t offset;
    enum reloc_type type;
};

typedef bool (*nexttok_t)(token *);

void init_parse();
bool parse(nexttok_t nexttok);
struct map *get_parse_section_map();
