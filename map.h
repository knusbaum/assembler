
// Map imlemented as linked-list for simplicity.
// List implementation left as part of interface to
// simplify iteration of elements.
struct map {
    char *key;
    void *val;
    struct map *next;
};

struct map *map_make();
void *map_get(struct map *m, char *key);
void map_put(struct map *m, char *key, void *val);
void map_destroy(struct map *m);
struct map *map_get_head(struct map *m);
