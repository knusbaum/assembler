# This stuff doesn't do anything. It's just here to demonstrate
# sections. Probably will add some actual data soon.
section .data
hello:
    db "Hello World!", 0A 00
count_txt:
    db "Counting %d", 0A 00
hex_format:
    db "%x", 0A, 00
deadbeef:
    db DE AD BE EF
cafebabe:
    db CAFEBABE
errmsg:
    db "Something went wrong.", 00


## The actual program text.
section .text

global _start

badexit:
    push errmsg
    call puts
exit:
    mov 1 eax
    mov 0 ebx
    int 0x80

print_msg:
    mov 13, edx    # length
    mov hello, ecx # message ptr (It's on the stack)
    mov 1, ebx     # STDOUT
    mov 4, eax     # sys_write call number
    int 0x80       # int 80h

    push hello
    call puts
    pop eax

    mov [deadbeef], ecx
    push ecx
    push hex_format
    call printf
    add 8 esp

    ret

do_the_loop:
    mov 0 ecx
    cmp ecx 1
    je badexit
    
loop_begin:
    push ecx
    push count_txt
    call printf
    pop ecx
    pop ecx
    add 1 ecx
    cmp ecx 10
    jne loop_begin
    
    ret

_start:
    call print_msg
    call do_the_loop
    call foo
    jmp  exit
