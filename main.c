#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include "log.h"
#include "buffwriter.h"
#include "lexer.h"
#include "parser.h"
#include "elf.h"
#include "labellist.h"
#include "map.h"

int tokfd;

bool next_token(token *tok) {
    if(read_token(tokfd, tok)) {
        return true;
    }
    return false;
}

void print_usage() {
    fprintf(stderr, "Usage: kas [-v] [-o objfile] [file]\n");
}

int main(int argc, char **argv) {

    if(argc < 3) {
        print_usage();
        exit(1);
    }

    int opt;
    char *infile = NULL;
    char *outfile = NULL;
    while((opt = getopt(argc, argv, "o:v")) != -1) {
        switch(opt) {
        case 'o':
            outfile = optarg;
            break;
        case 'v':
            set_logging(true);
            break;
        }
    }
//    {
//        int i;
//        for(i = 0; i < argc; i++) {
//            printf("argv[%d]: [%s]\n", i, argv[i]);
//        }
//        printf("argc: [%d], optind: [%d], arg at optind: [%s]\n", argc, optind, argv[optind]);
//    }
    if(optind+1 != argc) {
        print_usage();
        exit(1);
    }
    infile = argv[optind];

    init_parse();

    // Do all the parsing.
    tokfd = open(infile, O_RDONLY);
    if(tokfd == -1) {
        perror("Failed to open file");
        exit(1);
    }
    while(parse(next_token));
    close(tokfd);

    if(!is_eof()) {
        fprintf(stderr, "Something went wrong while parsing file. Exiting now.\n");
        exit(1);
    }

    // Loop through the sections and build symbol table and section headers.
    struct map *sections = get_parse_section_map();
    size_t n_sections = 0;
    struct map *seccount = sections;
    size_t local_syms = 0;
    while(seccount) {
        n_sections++;
        struct parse_section *parse_section = seccount->val;
        info("Adding local symbols for section %s\n", parse_section->name);

        struct lab_off *curr = get_head(parse_section->lablist);
        while(curr) {
            if(curr->binding != sym_global) {
                local_syms++;
                add_symbol(curr->lab, curr->off, 0, curr->binding, sym_notype, n_sections);
                //add_symbol(curr->lab, curr->off, 0, sym_global, sym_notype, n_sections);
                if(curr == curr->next) {
                    fprintf(stderr, "There's been a terrible error. Symbol list is circular.\n");
                    exit(1);
                }
            }
            curr = curr->next;
        }
        seccount = seccount->next;
    }

    n_sections = 0;
    seccount = sections;
    while(seccount) {
        n_sections++;
        struct parse_section *parse_section = seccount->val;
        info("Adding global symbols for section %s\n", parse_section->name);

        struct lab_off *curr = get_head(parse_section->lablist);
        while(curr) {
            if(curr->binding == sym_global) {
                add_symbol(curr->lab, curr->off, 0, curr->binding, sym_notype, n_sections);
                if(curr == curr->next) {
                    fprintf(stderr, "There's been a terrible error. Symbol list is circular.\n");
                    exit(1);
                }
            }
            curr = curr->next;
        }
        seccount = seccount->next;
    }

    section *s = malloc(n_sections * sizeof (section));
    section *s_ptr = s;

    size_t secnum = 1;
    while(sections) {
        if(!sections->val) continue;
        struct parse_section *parse_section = sections->val;
        info("Gathering metadata for section %s\n", parse_section->name);

        struct map *curr = map_get_head(parse_section->relocs);
        while(curr) {
            struct reloc *rel = curr->val;
            info("Adding reloc %s at offset %u\n", curr->key, rel->offset);
            if(!add_reloc(curr->key, rel->offset, rel->type)) {
                exit(1);
            }
            curr = curr->next;
        }

        secnum++;

        s_ptr->name = strdup(parse_section->name);
        s_ptr->type = TYPE_PROGBITS;
        s_ptr->flags = SHF_ALLOC | SHF_EXECINSTR;
        s_ptr->contents = parse_section->wb.buff;
        s_ptr->length = parse_section->wb.buff_ptr - parse_section->wb.buff;
        s_ptr++;
        sections = sections->next;
    }

    // Write all the things.
    FILE *f = fopen(outfile, "w");
    if(!f) {
        perror("Failed to open file");
        exit(1);
    }
    write_elf_header(f, s, n_sections, local_syms);
    fclose(f);
    return 0;
}
