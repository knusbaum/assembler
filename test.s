extern puts
global _start

section .data
somethingelsebefore: db "Hello somethingelsebefore\n", 0
words: db "hello, World", 10, 0
deadbeef: db 0xde, 0xad, 0xbe, 0xef

section .text

exit:
    mov   eax, 1
    mov   ebx, 0
    int   0x80

print_msg:
    mov   edx, 13
    mov   ecx, words
    mov   ebx, 1
    mov   eax, 4
    int   0x80

    push  words
    call  puts
    pop   eax
    ret

_start:

    call  print_msg
    jmp   exit
