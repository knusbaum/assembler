#include "map.h"
#include <stdlib.h>
#include <string.h>

struct map *map_make() {
    struct map *m = malloc(sizeof (struct map));
    m->key = NULL;
    m->val = NULL;
    m->next = NULL;
    return m;
}

void *map_get(struct map *m, char *key) {
    struct map *curr = m;
    while(curr) {
        if(strcmp(key, curr->key) == 0) {
            return curr->val;
        }
        curr = curr->next;
    }
    return NULL;
}

void map_put(struct map *m, char *key, void *val) {
    key = strdup(key);
    if(m->key == NULL) {
        m->key = key;
        m->val = val;
        return;
    }
    struct map *newm = malloc(sizeof (struct map));
    newm->key = key;
    newm->val = val;
    newm->next = m->next;
    m->next = newm;
}

void map_destroy(struct map *m) {
    struct map *next, *curr;
    curr = m;
    while(curr) {
        next = curr->next;
        free(curr->key);
        free(curr);
        curr = next;
    }
}

struct map *map_get_head(struct map *m) {
    if(m && m->key) {
        return m;
    }
    return NULL;
}
