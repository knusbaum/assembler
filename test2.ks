section .data
footxt:
    db "Foo!", 00
hello:
    db "hello again", 00


section .text

global foo

foo:
    push footxt
    call puts
    push hello
    call puts
    pop eax
    pop eax
    ret
