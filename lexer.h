enum toktype {
    LABEL,
    LABEL_D,
    LABELDEF,
    COMMAND,
    REGISTER,
    REGISTER_D,
    IMMEDIATE,
    NEWLINE,
    DATA      // Used for data commands -> db "Hello world!"
};

enum commands {
    ADD,
    SUB,
    MOV,
    JMP,
    CMP,
    JNE,
    JE,
    PUSH,
    POP,
    CALL,
    RET,
    INT,
    SHL,
    SECTION,
    // DB accepts strings and hex literals
    // DB strings can span multiple lines, but newlines are interpreted
    // literally (they end up in the data)
    // DB hex literals must be in two digit pairs optionally separated by
    // whitespace (no newlines)
    DB, // Never actually used. Comes in as toktype DATA
    GLOBAL
};

enum registers {
    EAX,
    EBX,
    ECX,
    EDX,
    ESI,
    EDI,
    ESP,
    EBP
};

typedef struct {
    enum toktype type;
    union {
        char *label;
        enum commands command;
        enum registers reg;
        uint32_t data;
        writable_buffer *buffer; // For toktype BUFFER
    };
} token;

/**
 * Reads a token into the spot pointed to by token from the file descriptor fd.
 * Tokens of type LABEL must have their label pointers freed.
 * returns true on success, false otherwise.
 *
 * On failure, check errno. If the stream is okay, there was a lexing error on that line.
 * Diagnostics will be output. Either abort scanning or skip forward to the next line.
 */
bool read_token(int fd, token *tok);
void info_token(token *tok);
void print_token(token *tok);
unsigned int currline();
bool is_eof();
