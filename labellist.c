#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "buffwriter.h"
#include "lexer.h"
#include "parser.h"
#include "elf.h"
#include "labellist.h"

///struct lab_off {
///    char *lab;
///    size_t off;
///    struct lab_off *next;
///};

//struct lab_off *head;

struct lab_off *lablist_make() {
    struct lab_off *l = malloc(sizeof (struct lab_off));
    l->lab = NULL;
    l->off = 0;
    l->next = NULL;
    return l;
}

struct lab_off *get_head(struct lab_off *l) {
    if(l->lab)
        return l;
    return NULL;
}

ssize_t offset_for_label(struct lab_off *l, char *label) {
    if(l->lab == NULL) return -1;
    struct lab_off *current = l;
    while(current) {
        if(strcmp(label, current->lab) == 0) {
            return current->off;
        }
        current = current->next;
    }
    return -1;
}

void set_offset_for_label(struct lab_off *l, char *label, size_t off, enum sym_binding binding) {
    struct lab_off *new;
    if(l->lab == NULL) {
        l->lab = strdup(label);
        l->off = off;
        l->binding = binding;
        return;
    }
    struct lab_off *curr = l;
    while(curr) {
        if(strcmp(label, curr->lab) == 0) {
            curr->off = off;
            if(binding > curr->binding) {
                curr->binding = binding;
            }
            return;
        }
        curr = curr->next;
    }

    // If one isn't found, make a new one.
    new = malloc(sizeof (struct lab_off));

    new->lab = strdup(label);
    new->off = off;
    new->binding = binding;
    new->next = l->next;
    l->next = new;
}

void lablist_destroy(struct lab_off *l) {
    struct lab_off *next, *curr;
    curr = l;
    while(curr) {
        next = curr->next;
        free(curr->lab);
        free(curr);
        curr = next;
    }
}
