
// This is just a standard dictionary. Will probably generify this later.

struct lab_off {
    char *lab;
    size_t off;
    enum sym_binding binding;
    struct lab_off *next;
};

struct lab_off *lablist_make();
ssize_t offset_for_label(struct lab_off *l, char *label);
void set_offset_for_label(struct lab_off *l, char *label, size_t off, enum sym_binding binding);
struct lab_off *get_head(struct lab_off *l);
void lablist_destroy(struct lab_off *l);
